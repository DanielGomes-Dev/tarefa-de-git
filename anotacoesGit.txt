Commit;
    #São alterações de Código  "Empacotado";

Branch:
    #Sequencia de commits
    #Possui uma Head, que é o commit mais recente da Branch;
    #Branch Padrão é Master;

Comandos Básicos:
    Help -> git help -> git help <comando> -> git <comandos> --help

    Init -> git init || git init <dir> -> Iniciar um repositório git
    
    Config -> git config --local user.name "Seu nome" , git config --local user.email seu@email.com

    Status -> git status -> ve o status do arquivos

    Add -> git add <dir/arq> -> adiciona arquivos a branch para rastrear ele;

    Commit -> git commit -m "Mensagem" -m "Descricao"; -> Envia para branch;

    Log -> git log, git log --all, git log --graph -> Vê todos os commits realizados

    Branch -> git branch ""-v"" -> mostra as branchs e qual é a atual;
           -> git branch nomeBranch -> (cria uma nova branch com o nome dado);
    
    Checkout -> git checkout ""-b"" nomeBranch (cria a branch e ja move para ela) -> Navegar entre Branches e Tags;

    Merge -> git merge <nomeBRanch> -> puxa as alterações da branch citada para a branch atual;


    Aqui vou fazer um teste de Merge que concerteza vai dar errado